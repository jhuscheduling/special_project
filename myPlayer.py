import game
import random


# function for basic picking strategy
# this simply fills next available spot from earliest to latest
# and picking home before away
def pick(g, teamNumber):
    # get number of slots
    numSlots = g.getNumSlots()
    homeCount = g.numHome(teamNumber)
    awayCount = g.numAway(teamNumber)
    condition = True
    homeCount = 0
    awayCount = 0


 


    counter = 0
    # while we havent set one yet
    while condition:
        # if not an off day
        if (not g.isSlotOff(counter)):
            # get home and away
            arrays = g.getSlotArrays(counter)
            home = arrays[0]
            away = arrays[1]
            # if open spot in home and team has more away series and team is not already plying in slot, then choose to be home
            if (home.count(0) > 0 and awayCount > homeCount and home.count(teamNumber) == 0 and away.count(teamNumber) == 0):
                hCondition = True
                hCount = 0
                # while we havent found it
                while hCondition:
                    if home[hCount] == 0:
                        pickSlot = g.getSlot(counter)
                        g.chooseHome(pickSlot, teamNumber, hCount)
                        hCondition = False
                        condition = False
                    else:
                        hCount = hCount + 1
            # if open spot in away and team has more home series and team is not already plying in slot, then choose to be away
            elif (away.count(0) > 0 and homeCount >= awayCount and home.count(teamNumber) == 0 and away.count(teamNumber) == 0):
                aCondition = True
                aCount = 0

                #decide what opponents are free to choose
                playTeams = home + away
                freeTeams = [1,2,3,4,5,6]
                freeTeams = [x for x in freeTeams if x not in playTeams]
                freeTeams.remove(teamNumber)

                while aCondition:
                    if away[aCount] == 0:
                        pickSlot = g.getSlot(counter)

                        #if there is no opponent yet for away team, choose one at random from free teams
                        if (home[aCount] == 0):
                            g.chooseAwayWithOpponent(pickSlot,teamNumber, random.choice(freeTeams), aCount)
                        else:
                            g.chooseAwayNoOpponent(pickSlot, teamNumber, aCount)
                            
                        aCondition = False
                        condition = False
                    else:
                        aCount = aCount + 1
            # if no openings in slot
            else:
                if counter >= numSlots - 1:
                    condition = False

        counter = counter + 1