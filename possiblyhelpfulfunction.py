# The following function returns a list of all possible moves for a team for their next pick
def getPossibleMoves(g,teamnumber,numTeams):
    # 1st col is the slot #, 2nd col is either H or A, 3rd col is index, 4th col is for picking opponent when away
    moves = []
    numSlots = g.getNumSlots()
    # Loop over all slots
    for i in range(numSlots):
        home = g.getSlotArrays(i)[0]
        away = g.getSlotArrays(i)[1]
        # If slot is off day, ignore
        if not g.isSlotOff(i):
            # If this team is already playing in the slot, ignore
            if teamnumber not in home and teamnumber not in away:
                # If 0 is in the home array, we can therefore play at home in this slot
                if 0 in home:
                    # List to keep track of indices
                    ilist = []
                    # Find all indices of 0 in home
                    for k in range(0,len(home)):
                        if home[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, 0] to moves
                    for k in ilist:
                        moves.append([i,'H',k,0])
                # If 0 is in the away array, we can play away in this slot
                if 0 in away:
                    ilist = []
                    # Find all indices of 0 in away
                    for k in range(0, len(away)):
                        if away[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, hometeam] to moves
                    for k in ilist:
                        # If there is home team in this index, that team will be the 4th column
                        if home[k] != 0:
                            moves.append([i, 'A', k,home[k]])
                        # If not, we loop through teams and append those who are not already playing in this slot
                        if home[k] == 0:
                            for r in range(1,numTeams+1):
                                if r not in home and r not in away and r != teamnumber:
                                    moves.append([i, 'A', k,r])
    return moves