# The following function returns a list of all possible moves for a team for their next pick
def getPossibleMoves(g,teamnumber,numTeams):
    # 1st col is the slot #, 2nd col is either H or A, 3rd col is index, 4th col is for picking opponent when away
    moves = []
    numSlots = g.getNumSlots()
    # Loop over all slots
    for i in range(numSlots):
        home = g.getSlotArrays(i)[0]
        away = g.getSlotArrays(i)[1]
        # If slot is off day, ignore
        if not g.isSlotOff(i):
            # If this team is already playing in the slot, ignore
            if teamnumber not in home and teamnumber not in away:
                # If 0 is in the home array, we can therefore play at home in this slot
                if 0 in home:
                    # List to keep track of indices
                    ilist = []
                    # Find all indices of 0 in home
                    for k in range(0,len(home)):
                        if home[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, 0] to moves
                    for k in ilist:
                        moves.append([i,'H',k,0])
                # If 0 is in the away array, we can play away in this slot
                if 0 in away:
                    ilist = []
                    # Find all indices of 0 in away
                    for k in range(0, len(away)):
                        if away[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, hometeam] to moves
                    for k in ilist:
                        # If there is home team in this index, that team will be the 4th column
                        if home[k] != 0:
                            moves.append([i, 'A', k,home[k]])
                        # If not, we loop through teams and append those who are not already playing in this slot
                        if home[k] == 0:
                            for r in range(1,numTeams+1):
                                if r not in home and r not in away and r != teamnumber:
                                    moves.append([i, 'A', k,r])
    return moves


# Function to count 3 window violations caused by move--associated to C1
def count3WindowViolations(currentgame, teamnumber, pick):
    violations = 0
    # There are 3 different ways that a move can cause a 3 window violation, we check all 3
    # First we need to make a list of non-offday slots
    numslots = currentgame.getNumSlots()
    onslots = []
    for i in range(numslots):
        if not currentgame.isSlotOff(i):
            onslots.append(i)
    # Find index of slot in onslots
    index = onslots.index(pick[0])
    maxindex = len(onslots)-1
    # Check back 2 slots from the slot we pick in
    if index-2 >= 0:
        # If we pick home, check if this causes 3 home series in a row
        home1 = currentgame.getSlotArrays(onslots[index-2])[0]
        home2 = currentgame.getSlotArrays(onslots[index-1])[0]
        away1 = currentgame.getSlotArrays(onslots[index - 2])[1]
        away2 = currentgame.getSlotArrays(onslots[index - 1])[1]
        if pick[1] == 'H':
            if teamnumber in home1 and teamnumber in home2:
                violations = violations+1
        if pick[1] == 'A':
            if teamnumber in away1 and teamnumber in away2:
                violations = violations + 1
    # Check back 1 and forward 1
    if index-1 >= 0 and index+1 <= maxindex:
        home1 = currentgame.getSlotArrays(onslots[index - 1])[0]
        home2 = currentgame.getSlotArrays(onslots[index + 1])[0]
        away1 = currentgame.getSlotArrays(onslots[index - 1])[1]
        away2 = currentgame.getSlotArrays(onslots[index + 1])[1]
        if pick[1] == 'H':
            if teamnumber in home1 and teamnumber in home2:
                violations = violations + 1
        if pick[1] == 'A':
            if teamnumber in away1 and teamnumber in away2:
                violations = violations + 1
    # Check forward 2 slots
    if index + 2 <= maxindex:
        home1 = currentgame.getSlotArrays(onslots[index + 1])[0]
        home2 = currentgame.getSlotArrays(onslots[index + 2])[0]
        away1 = currentgame.getSlotArrays(onslots[index + 1])[1]
        away2 = currentgame.getSlotArrays(onslots[index + 2])[1]
        if pick[1] == 'H':
            if teamnumber in home1 and teamnumber in home2:
                violations = violations + 1
        if pick[1] == 'A':
            if teamnumber in away1 and teamnumber in away2:
                violations = violations + 1
    return violations

# Function that counts home-away balance violations
def countHABalanceViolations(currentgame, teamnumber, pick):
    # Loop over all slots and count how many home and away series we have
    homecount = 0
    awaycount = 0
    violations = 0
    numslots = currentgame.getNumSlots()
    for i in range(numslots):
        if not currentgame.isSlotOff(i):
            home = currentgame.getSlotArrays(i)[0]
            away = currentgame.getSlotArrays(i)[1]
            if teamnumber in home:
                homecount = homecount + 1
            if teamnumber in away:
                awaycount = awaycount + 1
    # If we pick a home series and we are already playing more home series, we incur a penalty
    if pick[1] == 'H':
        if homecount > awaycount:
            violations = violations + (homecount - awaycount)
    # If we pick an away series and we are already playing more away series, we incur a penalty
    if pick[1] == 'A':
        if awaycount > homecount:
            violations = violations + (awaycount - homecount)
    return violations

# Function controlling opponent distribution--associated with C3
def countOpponentDistributionViolations(currentgame, teamnumber, pick):
    # Fundamentally the same code as countHABalanceViolations
    violations = 0
    numteams = len(currentgame.getSlotArrays(0)[0])*2
    numslots = currentgame.getNumSlots()
    teamlist = []
    for i in range(1,numteams+1):
        if i != teamnumber:
            teamlist.append(i)
    # Count how many times we play each team
    countlist = []
    for j in teamlist:
        counter = 0
        for i in range(numslots):
            if not currentgame.isSlotOff(i):
                home = currentgame.getSlotArrays(i)[0]
                away = currentgame.getSlotArrays(i)[1]
                if teamnumber in home and j in away:
                    if home.index(teamnumber) == away.index(j):
                        counter = counter + 1
                if teamnumber in away and j in home:
                    if home.index(j) == away.index(teamnumber):
                        counter = counter + 1
        countlist.append(counter)
    # If we choose H, check if we are playing anyone away and count violations
    if pick[1] == 'H':
        away = currentgame.getSlotArrays(pick[0])[1]
        if away[pick[2]] > 0:
            # Locate the corresponding index in teamlist/countlist
            index = teamlist.index(away[pick[2]])
            # Find min in countlist
            minplayed = min(countlist)
            violations = violations + (countlist[index] - minplayed)
    if pick[1] == 'A':
        index = teamlist.index(pick[3])
        minplayed = min(countlist)
        violations = violations + (countlist[index] - minplayed)
    return violations

# Function controlling opponent H/A distribution (want to play each opponent H and A even amount of times)--C4
def countOpponentHADistributionViolations(currentgame, teamnumber, pick):
    # Essentially same code as countHABalanceViolations and countOpponentDistributionViolations
    violations = 0
    numteams = len(currentgame.getSlotArrays(0)[0]) * 2
    numslots = currentgame.getNumSlots()
    teamlist = []
    for i in range(1, numteams + 1):
        if i != teamnumber:
            teamlist.append(i)
    # Count how many times we play each team H/A
    countlist = []
    for j in teamlist:
        homecount = 0
        awaycount = 0
        for i in range(numslots):
            if not currentgame.isSlotOff(i):
                home = currentgame.getSlotArrays(i)[0]
                away = currentgame.getSlotArrays(i)[1]
                if teamnumber in home and j in away:
                    if home.index(teamnumber) == away.index(j):
                        homecount = homecount + 1
                if teamnumber in away and j in home:
                    if home.index(j) == away.index(teamnumber):
                        awaycount = awaycount + 1
        countlist.append([homecount,awaycount])
    # Now add violations accordingly
    if pick[1] == 'A':
        index = teamlist.index(pick[3])
        if countlist[index][1] > countlist[index][0]:
            violations = violations + (countlist[index][1] - countlist[index][0])
    if pick[1] == 'H':
        away = currentgame.getSlotArrays(pick[0])[1]
        if away[pick[2]] > 0:
            index = teamlist.index(away[pick[2]])
            if countlist[index][0] > countlist[index][1]:
                violations = violations + (countlist[index][0] - countlist[index][1])
    return violations


# Function that calculates list of penalties for each move in next move
def calcPenaltyList(currentgame, teamnumber, moves, C1, C2, C3,C4):
    # penaltylist has form: [[move], penalty]
    penaltylist = []
    # Loop over each possible move
    for i in moves:
        # Calculate penalties
        p1 = count3WindowViolations(currentgame, teamnumber, i)
        p2 = countHABalanceViolations(currentgame, teamnumber, i)
        p3 = countOpponentDistributionViolations(currentgame, teamnumber, i)
        p4 = countOpponentHADistributionViolations(currentgame, teamnumber, i)
        penalty = p1*C1 + p2*C2 + p3*C3 + p4*C4
        penaltylist.append([i,penalty])
    return penaltylist

# Function that does the picking
def pick(g,teamnumber,numTeams, C1,C2,C3,C4):
    currentgame = g
    moves = getPossibleMoves(currentgame,teamnumber,numTeams)
    penaltylist = calcPenaltyList(currentgame, teamnumber, moves, C1, C2, C3,C4)
    penalties = []
    for i in penaltylist:
        penalties.append(i[1])
    minindex = penalties.index(min(penalties))
    bestmove = penaltylist[minindex][0]
    if bestmove[1] == 'H':
        g.chooseHome(g.getSlot(bestmove[0]), teamnumber, bestmove[2])
    if bestmove[1] == 'A':
        g.chooseAwayWithOpponent(g.getSlot(bestmove[0]), teamnumber, bestmove[3], bestmove[2])
