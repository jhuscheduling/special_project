import calendar

# game class represents the game
class Game:

    # initialize game from the text file
    def __init__(self, filename, numTeams):
        # read txt file
        file1 = open(filename, 'r')
        lines = file1.readlines()
        file1.close()

        # field that holds all slots in a list
        self.slotArray = []
        # set start date from first line in array
        beginDate = lines[0]
        dateArray = beginDate.split("-")
        for i in range(len(dateArray)):
            dateArray[i] = int(dateArray[i])
        dayIndex = (calendar.monthrange(dateArray[0], dateArray[1])[0] + dateArray[2] - 1) % 7

        lines.pop(0)
        # loop through values for slot length
        for i in range(len(lines)):
            # if sign is negative its an off day
            offDay = False
            numGames = int(lines[i])
            if (numGames < 0):
                offDay = True
            numGames = abs(numGames)
            # give slots a name, not sure if needed ?
            slotName = "slot" + str(i)
            # slotDays binary array of weekdays
            # monday is 0 index in this array
            slotDays = [0,0,0,0,0,0,0]
            # slot dates is of length N and holds dates of games
            # in YYYY-MM-DD format
            slotDates = []
            # loop over number of games
            for i in range(numGames):
                # extract year, month and day
                year = dateArray[0]
                month = dateArray[1]
                day = dateArray[2]
                # append day toi slotDates
                slotDates.append(str(dateArray[0])+"-"+str(dateArray[1])+"-"+str(dateArray[2]))
                # at day index set 1 (say this day is a game)
                slotDays[dayIndex] = 1
                # if end of month start new month
                if day == calendar.monthrange(year, month)[1]:
                    dateArray[1] += 1
                    dateArray[1] = dateArray[1] % 12
                    dateArray[2] = 1
                # else add one to day
                else:
                    dateArray[2] += 1
                # go to next weekday
                dayIndex += 1
                dayIndex = dayIndex % 7
            # TODO: fix slot constructor to have dates and days
            currentSlot = Slot(slotName, numGames, offDay, numTeams,slotDays,slotDates)
            self.slotArray.append(currentSlot)

    # function for player client to choose home
    def chooseHome(self, slot, teamNumber, index):
        slot.setHome(teamNumber, index)

    # function for player client to choose away
    def chooseAwayNoOpponent(self, slot, teamNumber, index):
        slot.setAway(teamNumber, index)

    # function for player client to choose away while also specifying home opponent
    def chooseAwayWithOpponent(self, slot, teamNumber, opponentTeamNumber, index):
        slot.setAway(teamNumber, index)
        slot.setHome(opponentTeamNumber, index)

    # function for player client to access slot object
    def getSlot(self, index):
        return self.slotArray[index]

    # function for player client to get number of slots
    def getNumSlots(self):
        return len(self.slotArray)

    # function for player client to access home/away for a slot
    def getSlotArrays(self, slotNumber):
        return self.slotArray[slotNumber].getArrays()

    # function for player client to see if a slot is an off day
    def isSlotOff(self, slotNumber):
        return self.slotArray[slotNumber].getOffDay()

    def numWeekendDays(self, slotNumber):
        if (not self.slotArray[slotNumber].getOffDay()):
            days = self.slotArray[slotNumber].getDays()
            return (days[4] + days[5])
        else:
            return 0

    def totalWeekendDays(self):
        counter = 0
        num = 0
        while (counter < (self.getNumSlots())):
            if(not self.isSlotOff(counter)):
                days = self.slotArray[counter].getDays()
                num = num + self.numWeekendDays(counter)
                counter = counter + 1
            else:
                counter = counter + 1
        return num

    def numHome(self, teamNumber):
        counter = 0
        homeCount = 0
        while (counter < self.getNumSlots()):
            if (not self.isSlotOff(counter)):
                arrays = self.getSlotArrays(counter)
                home = arrays[0]
                if (home.count(teamNumber) == 1):
                    homeCount = homeCount + 1
                    counter = counter + 1
                else:
                    counter = counter + 1
            else:
                counter = counter + 1
        return homeCount

    def numAway(self, teamNumber):
        counter = 0
        awayCount = 0
        while (counter < self.getNumSlots()):
            if (not self.isSlotOff(counter)):
                arrays = self.getSlotArrays(counter)
                away = arrays[1]
                if (away.count(teamNumber) == 1):
                    awayCount = awayCount + 1
                    counter = counter + 1
                else:
                    counter = counter + 1
            else:
                counter = counter + 1
        return awayCount

    # function that prints home/away for each slot -- for testing purposes
    def printSchedule(self):
        for s in self.slotArray:
            print(s.getSName())
            print(s.getArrays()[0])
            print(s.getArrays()[1])
            print(s.getDays())
            print(s.getDates())


# slot class represents each slot
class Slot:
    # constructor
    def __init__(self, slotName, numGames, offDay, numTeams,slotDays,slotDates):
        self.slotName = slotName
        self.numGames = numGames
        self.offDay = offDay
        self.slotDays = slotDays
        self.slotDates = slotDates
        self.homeArray = [0] * int(numTeams / 2)
        self.awayArray = [0] * int(numTeams / 2)

    # allows game to access home and away for player client
    def getArrays(self):
        return self.homeArray, self.awayArray

    # returns slot name
    def getSName(self):
        return self.slotName

    # allows game to access if slot is an off day
    def getOffDay(self):
        return self.offDay

    # allows game client to access days of week slot occupies
    def getDays(self):
        return self.slotDays

    # allows game client to access calendar dates slot contains
    def getDates(self):
        return self.slotDates

    # allows game to set home for a slot
    def setHome(self, teamNumber, index):
        self.homeArray[index] = teamNumber

    # allows game to set away for slot
    def setAway(self, teamNumber, index):
        self.awayArray[index] = teamNumber

