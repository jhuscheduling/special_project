from game import Game
import BenPlayer
import LoganPlayerNew
import myPlayer
import testplayer
import csv

def getPossibleMoves(g,teamnumber,numTeams):
    # 1st col is the slot #, 2nd col is either H or A, 3rd col is index, 4th col is for picking opponent when away
    moves = []
    numSlots = g.getNumSlots()
    # Loop over all slots
    for i in range(numSlots):
        home = g.getSlotArrays(i)[0]
        away = g.getSlotArrays(i)[1]
        # If slot is off day, ignore
        if not g.isSlotOff(i):
            # If this team is already playing in the slot, ignore
            if teamnumber not in home and teamnumber not in away:
                # If 0 is in the home array, we can therefore play at home in this slot
                if 0 in home:
                    # List to keep track of indices
                    ilist = []
                    # Find all indices of 0 in home
                    for k in range(0,len(home)):
                        if home[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, 0] to moves
                    for k in ilist:
                        moves.append([i,'H',k,0])
                # If 0 is in the away array, we can play away in this slot
                if 0 in away:
                    ilist = []
                    # Find all indices of 0 in away
                    for k in range(0, len(away)):
                        if away[k] == 0:
                            ilist.append(k)
                    # Append [slotnumber, H, index, hometeam] to moves
                    for k in ilist:
                        # If there is home team in this index, that team will be the 4th column
                        if home[k] != 0:
                            moves.append([i, 'A', k,home[k]])
                        # If not, we loop through teams and append those who are not already playing in this slot
                        if home[k] == 0:
                            for r in range(1,numTeams+1):
                                if r not in home and r not in away and r != teamnumber:
                                    moves.append([i, 'A', k,r])
    return moves


# basic game client
# set number of teams and make a game
numTeams = 10
g = Game("140GameTemplate.txt", numTeams)
numSlots = g.getNumSlots()

# simulate all slots * teams picks
for i in range(numSlots * numTeams):
    # this imagines all players using basic pick scheme
    # so we just set player number here
    teamNumber = (i % numTeams) + 1
    check = getPossibleMoves(g, teamNumber, numTeams)
    if len(check) > 0:
        LoganPlayerNew.pick(g, teamNumber, numTeams, 1, 1, 1, 1, 20, 20)

g.printSchedule()

# Create list to write into csv
# Format: list of slots: [team 1, ... team n, numgames in slot] (same as normal)
slotlist = []
for i in range(numSlots):
    teamlist = []
    if not g.isSlotOff(i):
        home = g.getSlotArrays(i)[0]
        away = g.getSlotArrays(i)[1]
        for j in range(1,numTeams+1):
            # If team is home, append to teamlist
            if j in home:
                teamlist.append(j)
            if j in away:
                index = away.index(j)
                teamlist.append(home[index])
        days = g.getSlot(i).getDays()
        counter = 0
        for r in days:
            if r == 1:
                counter = counter +1
        teamlist.append(counter)
    else:
        for j in range(1,numTeams+1):
            teamlist.append(0)
        teamlist.append(1)
    slotlist.append(teamlist)
with open('schedule5.csv', 'w') as file:
    writer = csv.writer(file)
    for k in slotlist:
        writer.writerow(k)


