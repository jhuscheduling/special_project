import game


# function for basic picking strategy
# this simply fills next available spot from earliest to latest
# and picking home before awat
def pick(g, teamNumber):
    # get number of slots
    numSlots = g.getNumSlots()
    counter = 0
    condition = True
    # while we havent set one yet
    while condition:
        # if not an off day
        if (not g.isSlotOff(counter)):
            # get home and away
            arrays = g.getSlotArrays(counter)
            home = arrays[0]
            away = arrays[1]
            # if open spot in home
            if (home.count(0) > 0):
                hCondition = True
                hCount = 0
                # while we havent found it
                while hCondition:
                    if home[hCount] == 0:
                        pickSlot = g.getSlot(counter)
                        g.chooseHome(pickSlot, teamNumber, hCount)
                        hCondition = False
                        condition = False
                    else:
                        hCount = hCount + 1
            # if no open in home but in away
            elif (away.count(0) > 0):
                aCondition = True
                aCount = 0
                while aCondition:
                    if away[aCount] == 0:
                        pickSlot = g.getSlot(counter)
                        g.chooseAwayNoOpponent(pickSlot, teamNumber, aCount)
                        aCondition = False
                        condition = False
                    else:
                        aCount = aCount + 1
            # if no openings in slot
            else:
                if counter >= numSlots - 1:
                    condition = False

        counter = counter + 1
